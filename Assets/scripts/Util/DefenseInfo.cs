﻿using UnityEngine;

public class DefenseInfo : MonoBehaviour {

    public Animator animator;

	void OnTriggerEnter2D(Collider2D other)
	{
        if (other.tag == "Attack")
        {
            if(other.name.Contains("Low"))
                animator.SetTrigger("_IsHitLow");
            else
                animator.SetTrigger("_IsHit");
        }

        if (other.tag == "Grab")
        {
            animator.SetTrigger("_GotGrabbed");
        }
	}

    void OnTriggerExit2D(Collider2D other)
    {
        
    }
}
