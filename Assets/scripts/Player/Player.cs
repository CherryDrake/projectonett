﻿﻿﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 5;

	public float maxJumpHeight = 1;
    public float minJumpHeight = 0.25f;
    public float timeToJumpApex = .4f;
	float accelerationTimeAirborn = .2f;

	Vector3 velocity;
	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	float velocityXSmoothing;

	public Controller2D controller;

	Vector2 directionalInput;

	//Attack Variables
    float actionTimer;
    float actionCooldown = .50f;

    //Player Object
    public Player player;

    //Controller Inputs
    KeyCode BUTTON_A = KeyCode.JoystickButton0;
    KeyCode BUTTON_B = KeyCode.JoystickButton1;
    KeyCode BUTTON_X = KeyCode.JoystickButton2;
    KeyCode BUTTON_Y = KeyCode.JoystickButton3;
    //KeyCode BUTTON_L = KeyCode.JoystickButton4;
    //KeyCode BUTTON_R = KeyCode.JoystickButton5;
    //KeyCode BUTTON_SELECT = KeyCode.JoystickButton6;
    //KeyCode BUTTON_START = KeyCode.JoystickButton7;

    //State Machine Variables
    bool isGrounded, isBlocking, isHit, inCombat;

    public Animator stateMachine;

	void Start () {
        player = GetComponent<Player>();
		controller = GetComponent<Controller2D>();
		
        gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2) / 5;
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt(Mathf.Abs(gravity) * minJumpHeight) / 2;
	}

	void Update(){
        actionTimer += Time.deltaTime;

		stateMachine.SetBool("IsMoving", velocity.x != 0);
		stateMachine.SetBool("IsGrounded", controller.collisions.below);

        isBlocking = stateMachine.GetBool("_Block");
        isHit = stateMachine.GetBool("_IsHit");
        isGrounded = stateMachine.GetBool("IsGrounded");
        inCombat = stateMachine.GetBool("InCombat");

        CalculateVelocity();
        GetInput(); //Movement and Attack States

        //Debug

        Debug.Log("Directional Input x: " + directionalInput.x);

		if (Input.GetKeyDown(KeyCode.Alpha9))
		{
            stateMachine.SetInteger("Health", -80);
			stateMachine.SetTrigger("_IsHit");
		}
	}

	void GetInput (){
        //Movement
        if (inCombat)
        {
            directionalInput = Vector2.zero;
        }
        else
        {
			directionalInput =
				new Vector2(
					Input.GetAxisRaw("Horizontal"),
					Input.GetAxisRaw("Vertical"));    
        }

		controller.Move(velocity * Time.deltaTime, directionalInput);

        //Jumping
        if(isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(BUTTON_B))
            {
                velocity.y = maxJumpVelocity;
            }
        }else
        {
			if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(BUTTON_B))
			{
                Debug.Log("Let go!");
				if (velocity.y > minJumpVelocity)
				{
                    Debug.Log("Lower velocity");
					velocity.y = minJumpVelocity;
				}
			}
        }

        //Combat
        if (actionTimer > actionCooldown) //Ready for Action
        {
            if ((Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(BUTTON_A))
                && (!isHit || stateMachine.GetCurrentAnimatorStateInfo(0).IsName("Holding")))
            {
                stateMachine.SetTrigger("_Attack");
                ResetTimer();
            }
            else if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(BUTTON_B))
            {
                //Todo: Determine a function
            }
            else if (Input.GetKeyDown(KeyCode.M) || Input.GetKeyDown(BUTTON_X))
            {
                stateMachine.SetBool("_Block", true);
            }
            else if (Input.GetKeyDown(KeyCode.N) || Input.GetKeyDown(BUTTON_Y))
            {
                stateMachine.SetTrigger("_GrabAttack");
;           }
		}

		//For super light taps
		if (Input.GetKeyUp(KeyCode.M) || Input.GetKeyUp(BUTTON_X))
		{
			stateMachine.SetBool("_Block", false);
		}

		//Determine Modifier
		if (Input.GetAxisRaw("Vertical") < 0)
			stateMachine.SetBool("_Low", true);
		else
			stateMachine.SetBool("_Low", false);
	}

	void CalculateVelocity(){
		
        float targetVelocityX = directionalInput.x * moveSpeed;
		
        velocity.x = Mathf.SmoothDamp
			(velocity.x * directionalInput.x == 0 ? 0 : directionalInput.x, 
			targetVelocityX, 
			ref velocityXSmoothing, accelerationTimeGrounded);
		if(velocity.x < 0.05 && velocity.x > -0.05)
			velocity.x = 0;

        if(!isGrounded)
            velocity.y += gravity * Time.deltaTime;
	}

    void ResetTimer(float time = 0f)
    {
        actionTimer = time;
    }

	void OnGUI()
	{
        var values = State.GetValues(typeof(State));
        string statename = "";
        foreach(State state in values)
        {
            if (stateMachine.GetCurrentAnimatorStateInfo(0).IsName(state.ToString()))
            {
                statename = state.ToString();
            }
            else
            {
                //statename = "None Found!";
            }
        }

		// Game labels
        GUI.Label(new Rect(10, 10, 300, 20), "Current State: " + statename);
        GUI.Label(new Rect(10, 30, 300, 20), "Health: " + stateMachine.GetInteger("Health"));
        GUI.Label(new Rect(10, 50, 300, 20), "Speed: " + stateMachine.speed);
	}	
}	