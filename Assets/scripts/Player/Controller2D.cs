﻿using UnityEngine;

public class Controller2D : RaycastController
{

	public float maxSlopeAngle = 55;
	public CollisionInfo collisions;
	[HideInInspector]
	public Vector2 playerInput;

	public override void Start()
	{
		base.Start();
		collisions.faceDir = 1;
	}

	//Overload for Platform movement
	public void Move(Vector2 moveAmount, bool standingOnPlatform)
	{
		Move(moveAmount, Vector2.zero, standingOnPlatform);
	}

	public void Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false)
	{
		UpdateRaycastOrigins();
		collisions.Reset();
		playerInput = input;

		if (moveAmount.x != 0)
		{
			collisions.faceDir = (int)Mathf.Sign(moveAmount.x);
		}

		HorizontalCollisions(ref moveAmount);

		if (moveAmount.y != 0)
		{
			VerticalCollisions(ref moveAmount);
		}

		transform.Translate(moveAmount);

		if (standingOnPlatform)
		{
			collisions.below = true;
		}
	}

	void HorizontalCollisions(ref Vector2 moveAmount)
	{
		float directionX = collisions.faceDir;
		float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

		if (Mathf.Abs(moveAmount.x) < skinWidth)
		{
			rayLength = 2 * skinWidth;
		}

		for (int i = 0; i < horizontalRayCount; i++)
		{
			//If player is moving left, Origin point is the bottom left. Otherwise, origin is bottom right.
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i);

			//Determine if raycasts are hitting something
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			//Draw Rays
			Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.blue);

			if (hit)
			{

				moveAmount.x = (hit.distance - skinWidth) * directionX;
				rayLength = hit.distance; //Adjust all ray lengths so object will not move towards anything further down

				collisions.left = directionX == -1;
				collisions.right = directionX == 1;

			}
		}
	}

	void VerticalCollisions(ref Vector2 moveAmount)
	{
		float directionY = Mathf.Sign(moveAmount.y);
		float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;
		for (int i = 0; i < verticalRayCount; i++)
		{
			//If player is moving down, Origin point is the bottom left. Otherwise, origin is top left.
			Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);

			//Determine if raycasts are hitting something
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

			//Draw Rays
			Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

			if (hit)
			{
				moveAmount.y = (hit.distance - skinWidth) * directionY;
				rayLength = hit.distance; //Adjust all ray lengths so object will not move towards anything further down

				collisions.below = directionY == -1;
				collisions.above = directionY == 1;
			}
		}
	}

    public struct CollisionInfo
	{
		public bool above, below;
		public bool left, right;

		public int faceDir;

		public void Reset()
		{
			above = below = false;
			left = right = false;
		}
	}
}