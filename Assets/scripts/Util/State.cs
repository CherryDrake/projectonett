﻿//Enums for Logic
public enum State
{
	Start,
	Idle,
	Run,
	Punch_High,
	Punch_Low,
	Hit_High,
	Hit_Low,
	Block_Low,
	Block_High,
	Knockdown_High,
    Knockdown_Low,
    Grabbed,
    GrabbedHit,
    Thrown,
    BreakFree
}
