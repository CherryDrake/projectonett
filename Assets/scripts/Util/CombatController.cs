﻿using UnityEngine;

public class CombatController : StateMachineBehaviour 
{
    float breakTimer;
    float timeToBreak;
    bool brokenFree;
    Animator player;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Start Attack
        animator.SetBool("InCombat", true);

        if (stateInfo.IsName("Grabbed"))
        {
            if (timeToBreak <= 0)
                timeToBreak = Random.Range(2f, 4f);

            if (animator.tag == "Enemy")
            {
                player = GameObject.FindWithTag("Player").GetComponent<Animator>();
            }
        }
    }
		

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		//End Attack
		animator.SetBool("InCombat", false);

        if (stateInfo.IsName("Grabbing"))
        {
            
        }

        //animator.transform.localPosition = new Vector2(animator.transform.localPosition.x, 0);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		if (animator.GetBool("IsGrounded") == true)
		{
			//Stop moving for Ground Attacks

		}

        if (stateInfo.IsName("Grabbing"))
        {
        }

		if (stateInfo.IsName("Grabbed"))
		{
            breakTimer += Time.deltaTime;
            if(breakTimer >= timeToBreak)
            {
                breakTimer = 0f;
                animator.SetTrigger("BreakFree");
                if (animator.tag == "Enemy")
                {
                    Debug.Log("Break Free?");
                    Debug.Log("Player Holding? " + player.GetCurrentAnimatorStateInfo(0).IsName("Holding"));
                    player.SetTrigger("BreakFree");
                }
            }
		}

        if (stateInfo.IsName("Block_Low") || stateInfo.IsName("Block_High"))
        {
            if(Input.GetKeyUp(KeyCode.M))
            {
                animator.SetBool("_Block", false);
            }
           
        }
    }
}
