using UnityEngine;

public class AttackInfo : MonoBehaviour {
    
    public Animator animator;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Hit")
		{
            if(tag == "Grab")
            {
                animator.SetTrigger("_GrabSuccess");
            }
		}
	}
}
